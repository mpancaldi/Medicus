namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddressStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ShippingAddresses", "Street", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.ShippingAddresses", "City", c => c.String(nullable: false, maxLength: 30));
            AlterColumn("dbo.ShippingAddresses", "State", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.ShippingAddresses", "ZIP", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ShippingAddresses", "ZIP", c => c.String(nullable: false));
            AlterColumn("dbo.ShippingAddresses", "State", c => c.String(nullable: false));
            AlterColumn("dbo.ShippingAddresses", "City", c => c.String(nullable: false));
            AlterColumn("dbo.ShippingAddresses", "Street", c => c.String(nullable: false));
        }
    }
}

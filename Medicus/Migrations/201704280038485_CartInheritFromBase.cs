namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CartInheritFromBase : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Carts");
            DropColumn("dbo.Carts", "RecordId");
            DropColumn("dbo.Carts", "DateCreated");
            AddColumn("dbo.Carts", "Id", c => c.Int(nullable: false, identity: true));
            AddColumn("dbo.Carts", "CreatedOn", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Carts", "LastModified", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Carts", "RowID", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddPrimaryKey("dbo.Carts", "Id");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Carts", "Id");
            AddColumn("dbo.Carts", "DateCreated", c => c.DateTime(nullable: false));
            AddColumn("dbo.Carts", "RecordId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Carts");
            DropColumn("dbo.Carts", "RowID");
            DropColumn("dbo.Carts", "LastModified");
            DropColumn("dbo.Carts", "CreatedOn");
            AddPrimaryKey("dbo.Carts", "RecordId");
        }
    }
}

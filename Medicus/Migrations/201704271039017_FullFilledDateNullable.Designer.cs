// <auto-generated />
namespace Medicus.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class FullFilledDateNullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FullFilledDateNullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201704271039017_FullFilledDateNullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

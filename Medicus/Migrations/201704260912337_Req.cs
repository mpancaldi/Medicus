namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Req : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderItems", "Medicine_Id", "dbo.Medicines");
            DropIndex("dbo.OrderItems", new[] { "Medicine_Id" });
            RenameColumn(table: "dbo.OrderItems", name: "Order_Id", newName: "OrderId");
            RenameColumn(table: "dbo.OrderItems", name: "Medicine_Id", newName: "MedicineId");
            RenameIndex(table: "dbo.OrderItems", name: "IX_Order_Id", newName: "IX_OrderId");
            AlterColumn("dbo.OrderItems", "MedicineId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderItems", "MedicineId");
            AddForeignKey("dbo.OrderItems", "MedicineId", "dbo.Medicines", "Id", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderItems", "MedicineId", "dbo.Medicines");
            DropIndex("dbo.OrderItems", new[] { "MedicineId" });
            AlterColumn("dbo.OrderItems", "MedicineId", c => c.Int());
            RenameIndex(table: "dbo.OrderItems", name: "IX_OrderId", newName: "IX_Order_Id");
            RenameColumn(table: "dbo.OrderItems", name: "MedicineId", newName: "Medicine_Id");
            RenameColumn(table: "dbo.OrderItems", name: "OrderId", newName: "Order_Id");
            CreateIndex("dbo.OrderItems", "Medicine_Id");
            AddForeignKey("dbo.OrderItems", "Medicine_Id", "dbo.Medicines", "Id");
        }
    }
}

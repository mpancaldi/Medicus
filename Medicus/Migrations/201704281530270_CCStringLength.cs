namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CCStringLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CreditCards", "FirstName", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.CreditCards", "LastName", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CreditCards", "LastName", c => c.String(nullable: false));
            AlterColumn("dbo.CreditCards", "FirstName", c => c.String(nullable: false));
        }
    }
}

namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FullFilledDateNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Orders", "Address_Id", "dbo.ShippingAddresses");
            DropForeignKey("dbo.Orders", "CreditCard_Id", "dbo.CreditCards");
            DropIndex("dbo.Orders", new[] { "Address_Id" });
            DropIndex("dbo.Orders", new[] { "CreditCard_Id" });
            AlterColumn("dbo.Orders", "FullFilledDate", c => c.DateTime());
            AlterColumn("dbo.Orders", "Address_Id", c => c.Int());
            AlterColumn("dbo.Orders", "CreditCard_Id", c => c.Int());
            CreateIndex("dbo.Orders", "Address_Id");
            CreateIndex("dbo.Orders", "CreditCard_Id");
            AddForeignKey("dbo.Orders", "Address_Id", "dbo.ShippingAddresses", "Id");
            AddForeignKey("dbo.Orders", "CreditCard_Id", "dbo.CreditCards", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "CreditCard_Id", "dbo.CreditCards");
            DropForeignKey("dbo.Orders", "Address_Id", "dbo.ShippingAddresses");
            DropIndex("dbo.Orders", new[] { "CreditCard_Id" });
            DropIndex("dbo.Orders", new[] { "Address_Id" });
            AlterColumn("dbo.Orders", "CreditCard_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "Address_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Orders", "FullFilledDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Orders", "CreditCard_Id");
            CreateIndex("dbo.Orders", "Address_Id");
            AddForeignKey("dbo.Orders", "CreditCard_Id", "dbo.CreditCards", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Orders", "Address_Id", "dbo.ShippingAddresses", "Id", cascadeDelete: true);
        }
    }
}

namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OneToManyEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carts",
                c => new
                    {
                        RecordId = c.Int(nullable: false, identity: true),
                        CartId = c.String(),
                        MedicineId = c.Int(nullable: false),
                        Count = c.Int(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RecordId)
                .ForeignKey("dbo.Medicines", t => t.MedicineId, cascadeDelete: false)
                .Index(t => t.MedicineId);
            
            CreateTable(
                "dbo.Reviews",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        MedicineId = c.Int(nullable: false),
                        Stars = c.Int(nullable: false),
                        Comment = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RowID = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CustomerId, cascadeDelete: false)
                .ForeignKey("dbo.Medicines", t => t.MedicineId, cascadeDelete: false)
                .Index(t => t.CustomerId)
                .Index(t => t.MedicineId);
            
            CreateTable(
                "dbo.CreditCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        CardNumber = c.String(nullable: false),
                        ExpireMonth = c.Int(nullable: false),
                        ExpireYear = c.Int(nullable: false),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RowID = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CustomerId, cascadeDelete: false)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.ShippingAddresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        Street = c.String(nullable: false),
                        City = c.String(nullable: false),
                        State = c.String(nullable: false),
                        ZIP = c.String(nullable: false),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RowID = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.CustomerId, cascadeDelete: false)
                .Index(t => t.CustomerId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerId = c.String(nullable: false, maxLength: 128),
                        FullFilledDate = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RowID = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Address_Id = c.Int(nullable: false),
                        CreditCard_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ShippingAddresses", t => t.Address_Id, cascadeDelete: false)
                .ForeignKey("dbo.CreditCards", t => t.CreditCard_Id, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CustomerId, cascadeDelete: false)
                .Index(t => t.CustomerId)
                .Index(t => t.Address_Id)
                .Index(t => t.CreditCard_Id);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Quantity = c.Int(nullable: false),
                        SubTotal = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CreatedOn = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        LastModified = c.DateTime(nullable: false, precision: 7, storeType: "datetime2"),
                        RowID = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Medicine_Id = c.Int(),
                        Order_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Medicines", t => t.Medicine_Id)
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: false)
                .Index(t => t.Medicine_Id)
                .Index(t => t.Order_Id);
            
            AddColumn("dbo.Medicines", "Manufacturer", c => c.String());
            AddColumn("dbo.Medicines", "CreatedOn", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Medicines", "LastModified", c => c.DateTime(nullable: false, precision: 7, storeType: "datetime2"));
            AddColumn("dbo.Medicines", "RowID", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.AspNetUsers", "DefaultAddress_Id", c => c.Int());
            AddColumn("dbo.AspNetUsers", "DefaultCreditCard_Id", c => c.Int());
            CreateIndex("dbo.AspNetUsers", "DefaultAddress_Id");
            CreateIndex("dbo.AspNetUsers", "DefaultCreditCard_Id");
            AddForeignKey("dbo.AspNetUsers", "DefaultAddress_Id", "dbo.ShippingAddresses", "Id");
            AddForeignKey("dbo.AspNetUsers", "DefaultCreditCard_Id", "dbo.CreditCards", "Id");
            DropColumn("dbo.Medicines", "Company");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Medicines", "Company", c => c.String());
            DropForeignKey("dbo.Carts", "MedicineId", "dbo.Medicines");
            DropForeignKey("dbo.Reviews", "MedicineId", "dbo.Medicines");
            DropForeignKey("dbo.Reviews", "CustomerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.ShippingAddresses", "CustomerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Orders", "CustomerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderItems", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.OrderItems", "Medicine_Id", "dbo.Medicines");
            DropForeignKey("dbo.Orders", "CreditCard_Id", "dbo.CreditCards");
            DropForeignKey("dbo.Orders", "Address_Id", "dbo.ShippingAddresses");
            DropForeignKey("dbo.AspNetUsers", "DefaultCreditCard_Id", "dbo.CreditCards");
            DropForeignKey("dbo.AspNetUsers", "DefaultAddress_Id", "dbo.ShippingAddresses");
            DropForeignKey("dbo.CreditCards", "CustomerId", "dbo.AspNetUsers");
            DropIndex("dbo.OrderItems", new[] { "Order_Id" });
            DropIndex("dbo.OrderItems", new[] { "Medicine_Id" });
            DropIndex("dbo.Orders", new[] { "CreditCard_Id" });
            DropIndex("dbo.Orders", new[] { "Address_Id" });
            DropIndex("dbo.Orders", new[] { "CustomerId" });
            DropIndex("dbo.ShippingAddresses", new[] { "CustomerId" });
            DropIndex("dbo.CreditCards", new[] { "CustomerId" });
            DropIndex("dbo.AspNetUsers", new[] { "DefaultCreditCard_Id" });
            DropIndex("dbo.AspNetUsers", new[] { "DefaultAddress_Id" });
            DropIndex("dbo.Reviews", new[] { "MedicineId" });
            DropIndex("dbo.Reviews", new[] { "CustomerId" });
            DropIndex("dbo.Carts", new[] { "MedicineId" });
            DropColumn("dbo.AspNetUsers", "DefaultCreditCard_Id");
            DropColumn("dbo.AspNetUsers", "DefaultAddress_Id");
            DropColumn("dbo.Medicines", "RowID");
            DropColumn("dbo.Medicines", "LastModified");
            DropColumn("dbo.Medicines", "CreatedOn");
            DropColumn("dbo.Medicines", "Manufacturer");
            DropTable("dbo.OrderItems");
            DropTable("dbo.Orders");
            DropTable("dbo.ShippingAddresses");
            DropTable("dbo.CreditCards");
            DropTable("dbo.Reviews");
            DropTable("dbo.Carts");
        }
    }
}

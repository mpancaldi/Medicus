namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigMedicine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Medicines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Ingredients = c.String(),
                        Category = c.Int(nullable: false),
                        ShortInfo = c.String(),
                        Description = c.String(),
                        ActiveAgent = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Medicines");
        }
    }
}

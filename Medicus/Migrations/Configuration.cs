namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Medicus.Models;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity;
    using System.Collections.Generic;
    using System.Threading;
    using System.Text.RegularExpressions;
    using System.Data.Entity.Validation;
    using System.Text;
    using System.Reflection;

    internal sealed class Configuration : DbMigrationsConfiguration<Medicus.Models.ApplicationDbContext>
    {
        private Random Rng = new Random();
        private List<Medicine> medicineList;
        private List<CreditCard> ccList;
        private List<ShippingAddress> addrList;
        private List<Order> orderList;
        private List<OrderItem> oderItemList;
        private List<Review> reviewList;

        public Configuration()
        {
            //AutomaticMigrationsEnabled = false;
            //AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Medicus.Models.ApplicationDbContext context)
        {

            //  This method will be called after migrating to the latest version.
            SeedUsers(context);
            SeedRoles(context);
            SeedMedicines(context);
            SeedCreditCards(context);
            SeedShippingAdresses(context);
            SeedReviews(context);
            SeedOrders(context);

            //must come last to give db time to create records
            AssignRoles(context);
        }

        private void SeedUsers(ApplicationDbContext context)
        {
            if (!(context.Users.Any(u => u.UserName == "overlord@med.it")))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                // create userList
                var userList = new List<ApplicationUser>()
                {
                    new ApplicationUser { UserName = "overlord@med.it", Email = "overlord@med.it", NameIdentifier = "overlord" },
                    new ApplicationUser { UserName = "marta@med.it", Email = "marta@med.it", NameIdentifier = "Marta Pancaldi" },
                    new ApplicationUser { UserName = "tahir@med.it", Email = "tahir@med.it", NameIdentifier = "Tahir Karim" },
                    new ApplicationUser { UserName = "werner@med.it", Email = "werner@med.it", NameIdentifier = "Werner Sperandio" },
                    new ApplicationUser { UserName = "sven@med.it", Email = "sven@med.it", NameIdentifier = "Sven Helmer" },
                    new ApplicationUser { UserName = "paolo@med.it", Email = "paolo@med.it", NameIdentifier = "Paolo Bolzoni" }
                };


                // create Users
                foreach (var usr in userList)
                {
                    if (userManager.FindByName(usr.UserName) == null)
                    {
                        // password is name part of email + 1234, e.g. paoloMMM@med.it => paoloMMM1234
                        userManager.Create(usr, Regex.Match(usr.UserName, "[^@]*") + "1234");
                    }

                }

            }
        }

        private void SeedRoles(ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // create Roles from RoleNames
            foreach (FieldInfo role in typeof(RoleNames).GetFields())
            {
                string roleName = role.GetRawConstantValue().ToString();

                if (!roleManager.RoleExists(roleName))
                {
                    roleManager.Create(new IdentityRole(roleName));
                }
            }
        }

        private void AssignRoles(ApplicationDbContext context)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            // assign roles to Users
            userManager.AddToRole(userManager.FindByName("overlord@med.it").Id, RoleNames.ADMINISTRATOR);
            userManager.AddToRole(userManager.FindByName("marta@med.it").Id, RoleNames.CUSTOMER);
            userManager.AddToRole(userManager.FindByName("werner@med.it").Id, RoleNames.CUSTOMER);
            userManager.AddToRole(userManager.FindByName("tahir@med.it").Id, RoleNames.CUSTOMER);
            userManager.AddToRole(userManager.FindByName("sven@med.it").Id, RoleNames.MANAGER);
            userManager.AddToRole(userManager.FindByName("paolo@med.it").Id, RoleNames.MANAGER);
        }
        #region SeedMedicines
        private void SeedMedicines(ApplicationDbContext context)
        {
            medicineList = new List<Medicine>() {
                new Medicine()
                {

                    Name = "Vicodine",
                    Drug = "hydrocodone/acetaminophen",
                    Manufacturer = "AbbVie Inc.",
                    Category = Category.PainReliever,
                    Indication = "pain",
                    ShortInfo = "Vicodin contains a combination of acetaminophen and hydrocodone.\n" +
                    "Hydrocodone is an opioid pain medication. An opioid is sometimes called a narcotic. " +
                    "Acetaminophen is a less potent pain reliever that increases the effects of hydrocodone.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),

                },
                new Medicine()
                {

                    Name = "Zocor",
                    Drug = "simvastatin",
                    Manufacturer = "Merck",
                    Category = Category.CholesterolReducer,
                    Indication = "high cholesterol",
                    ShortInfo = "Zocor is used to lower cholesterol and triglycerides (types of fat) in the blood.\n" +
                    "It is also used to lower the risk of stroke, heart attack, and other heart complications in people with diabetes," +
                    "coronary heart disease, or other risk factors.\nZocor is used in adults and children who are at least 10 years old.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Prinivil",
                    Drug = "lisinopril",
                    Manufacturer = "Merck",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "Lisinopril is an ACE inhibitor. ACE stands for angiotensin converting enzyme.\n" +
                    "Lisinopril is used to treat high blood pressure(hypertension) in adults and children who are at least 6 years old.\n" +
                    "Lisinopril is also used to treat congestive heart failure in adults, or to improve survival after a heart attack.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Synthroid",
                    Drug = "levothyroxine sodium",
                    Manufacturer = "AbbVie Inc.",
                    Category = Category.Other,
                    Indication = "hypothyroid",
                    ShortInfo = "Synthroid (levothyroxine) is a replacement for a hormone normally produced by your thyroid gland to regulate" +
                    "the body's energy and metabolism. Levothyroxine is given when the thyroid does not produce enough of this hormone on its own. " +
                    "Synthroid treats hypothyroidism (low thyroid hormone).",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),

                },
                new Medicine()
                {

                    Name = "Norvasc",
                    Drug = "amlodipine besylate",
                    Manufacturer = "Pfizer",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "Norvasc (amlodipine) is a calcium channel blocker that dilates (widens) blood vessels and improves blood flow. " +
                    "Norvasc is used to treat chest pain (angina) and other conditions caused by coronary artery disease. Norvasc is also used to treat " +
                    "high blood pressure (hypertension).",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Prilosec",
                    Drug = "omeprazole",
                    Manufacturer = "Procter & Gamble",
                    Category = Category.Other,
                    Indication = "acid reflux",
                    ShortInfo = "Prilosec (omeprazole) is a proton pump inhibitor that decreases the amount of acid produced in the stomach. " +
                    "Prilosec is used to treat symptoms of gastroesophageal reflux disease(GERD) and other conditions caused by excess stomach acid. " +
                    "Omeprazole is also used to promote healing of erosive esophagitis(damage to your esophagus caused by stomach acid). " +
                    "Prilosec may also be given together with antibiotics to treat gastric ulcer caused by infection with helicobacter pylori(H.pylori).\n" +
                    "Prilosec is not for immediate relief of heartburn symptoms.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Zithromax",
                    Drug = "azithromycin",
                    Manufacturer = "Pfizer",
                    Category = Category.AntiBiotic,
                    Indication = "antibiotic",
                    ShortInfo = "Zithromax (azithromycin) is an antibiotic that fights bacteria.Zithromax is used to treat many different types " +
                    "of infections caused by bacteria, such as respiratory infections, skin infections, ear infection and sexually transmitted " +
                    "diseases. Zithromax may also be used for purposes not listed in this medication guide.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Moxatag",
                    Drug = "amoxicillin",
                    Manufacturer = "Vernalis",
                    Category = Category.AntiBiotic,
                    Indication = "antibiotic",
                    ShortInfo = "Amoxicillin is a penicillin antibiotic that fights bacteria. Amoxicillin is used to treat many different " +
                    "types of infection caused by bacteria, such as tonsillitis, bronchitis, pneumonia, gonorrhea, and infections of the ear, " +
                    "nose, throat, skin, or urinary tract. Amoxicillin is also sometimes used together with another antibiotic called " +
                    "clarithromycin(Biaxin) to treat stomach ulcers caused by Helicobacter pylori infection.\nThis combination is sometimes " +
                    "used with a stomach acid reducer called lansoprazole(Prevacid).",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Glucophage",
                    Drug = "metformin HCL",
                    Manufacturer = "Merck",
                    Category = Category.Other,
                    Indication = "diabetes",
                    ShortInfo = "Glucophage (metformin) is an oral diabetes medicine that helps control blood sugar levels." +
                        "Glucophage is used to improve blood sugar control in people with type 2 diabetes.Glucophage is sometimes " +
                        "used in combination with insulin or other medications, but metformin is not for treating type 1 diabetes.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "HCTZ",
                    Drug = "hydrochlorothiazide",
                    Manufacturer = "Mylan Pharmaceuticals",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "HCTZ (hydrochlorothiazide) is a thiazide diuretic (water pill) that helps prevent your body from absorbing " +
                    "too much salt, which can cause fluid retention. HCTZ treats fluid retention (edema) in people with congestive heart failure, " +
                    "cirrhosis of the liver, or kidney disorders, or edema.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Xanax",
                    Drug = "alprazolam",
                    Manufacturer = "Pfizer",
                    Category = Category.AntiDepressant,
                    Indication = "anxiety",
                    ShortInfo = "Xanax (alprazolam) is a benzodiazepine. Alprazolam affects chemicals in the brain that may be unbalanced in people " +
                        "with anxiety.Xanax is used to treat anxiety disorders,  panic disorders, and anxiety caused by depression.Xanax may also be used " +
                        "for purposes not listed in this medication guide.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Lipitor",
                    Drug = "atorvastatin",
                    Manufacturer = "Pfizer",
                    Category = Category.CholesterolReducer,
                    Indication = "high cholesterol",
                    ShortInfo = "Lipitor(atorvastatin) belongs to a group of drugs called HMG CoA reductase inhibitors, " +
                        "or \"statins.\" Atorvastatin reduces levels of \"bad\" cholesterol(low - density lipoprotein, or LDL) " +
                        "and triglycerides in the blood, while increasing levels of \"good\" cholesterol(high - density lipoprotein, " +
                        "or HDL).Lipitor is used to treat high cholesterol, and to lower the risk of stroke, heart attack, or " +
                        "other heart complications in people with type 2 diabetes, coronary heart disease, or other risk factors.\n" +
                        "Lipitor is for use in adults and children who are at least 10 years old.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),

                },
                new Medicine()
                {

                    Name = "Lasix",
                    Drug = "furosemide",
                    Manufacturer = "Sanofi Aventis",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "Lasix (furosemide) is a loop diuretic (water pill) that prevents your body from absorbing too much salt. " +
                        "This allows the salt to instead be passed in your urine.Lasix is used to treat fluid retention(edema) in people with " +
                        "congestive heart failure, liver disease, or a kidney disorder such as nephrotic syndrome.Lasix is also used to treat " +
                        "high blood pressure(hypertension).",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Lopressor",
                    Drug = "metoprolol tartrate",
                    Manufacturer = "Novartis",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "Lopressor HCT is a beta - adrenergic blocking agent(beta - blocker) and diuretic combination. " +
                        "The beta - blocker works by slowing down the heartbeat, helping the heart beat more regularly, and reducing the " +
                        "amount of work the heart has to do.The diuretic increases the elimination of excess fluid, which helps to" +
                        " decrease blood pressure.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Ambien",
                    Drug = "zolpidem tartrate",
                    Manufacturer = "Sanofi",
                    Category = Category.Other,
                    Indication = "insomnia",
                    ShortInfo = "Ambien(zolpidem) is a sedative, also called a hypnotic. Zolpidem affects chemicals in the brain that " +
                        "may be unbalanced in people with sleep problems insomnia).Ambien is used to treat insomnia. The immediate-release tablet " +
                        "is used to help you fall asleep when you first go to bed. The extended-release form, Ambien CR, which has a first layer " +
                        "that dissolves quickly to help you fall asleep, and a second layer that dissolves slowly to help you stay asleep.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Tenormin",
                    Drug = "Atenolol",
                    Manufacturer = "ASTRAZENECA",
                    Category = Category.AntiHypertensive,
                    Indication = "high blood pressure",
                    ShortInfo = "Tenormin is a beta-blocker that affects the heart and circulation (blood flow through arteries and veins). " +
                        "Atenolol is used to treat angina(chest pain) and hypertension(high blood pressure).Atenolol is also used to lower the risk of death " +
                        "after a heart attack.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Zoloft",
                    Drug = "sertraline HCL",
                    Manufacturer = "Pfizer",
                    Category = Category.AntiDepressant,
                    Indication = "depression",
                    ShortInfo = "Zoloft (sertraline) is an antidepressant in a group of drugs called selective serotonin reuptake inhibitors (SSRIs). " +
                        "The way sertraline works is still not fully understood. It is thought to positively affect communication between nerve cells in " +
                        "the central nervous system and/or restore chemical balance in the brain.Zoloft is used to treat depression, obsessive-compulsive disorder, " +
                        "panic disorder, anxiety disorders, post-traumatic stress disorder (PTSD), and premenstrual dysphoric disorder (PMDD).",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Toprol",
                    Drug = "metoprolol succinate",
                    Manufacturer = "ASTRAZENECA",
                    Category = Category.AntiHypertensive,
                    Indication = "blood pressure",
                    ShortInfo = "Toprol-XL (metoprolol) is a beta-blocker that affects the heart and circulation (blood flow through arteries and veins). " +
                        "Toprol - XL is used to treat angina(chest pain) and hypertension(high blood pressure).It is also used to treat or prevent heart attack.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Celexa",
                    Drug = "Citalopram",
                    Manufacturer = "Allergan, Inc.",
                    Category = Category.AntiDepressant,
                    Indication = "depression",
                    ShortInfo = "Celexa (citalopram) is an antidepressant in a group of drugs called selective serotonin reuptake inhibitors (SSRIs). " +
                        "Celexa is used to treat depression.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Coumadin",
                    Drug = "warfarin sodium",
                    Manufacturer = "Bristol-Myers",
                    Category = Category.Other,
                    Indication = "blood thinner",
                    ShortInfo = "Coumadin(warfarin) is an anticoagulant (blood thinner).Warfarin reduces the formation of blood clots. " +
                        "Coumadin is used to treat or prevent blood clots in veins or arteries, which can reduce the risk of stroke, heart attack, " +
                        "or other serious conditions.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },

                new Medicine()
                {

                    Name = "Viagra",
                    Drug = "sildenafil",
                    Manufacturer = "Pfizer",
                    Category = Category.Other,
                    Indication = "erectile dysfunction",
                    ShortInfo = "Viagra(sildenafil) relaxes muscles found in the walls of blood vessels and increases blood flow to " +
                        "particular areas of the body. Viagra is used to treat erectile dysfunction(impotence) in men. Another brand of sildenafil " +
                        "is Revatio, which is used to treat pulmonary arterial hypertension and improve exercise capacity in men and women. Do not take " +
                        "Viagra while also taking Revatio, unless your doctor tells you to.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Januvia",
                    Drug = "sitagliptin",
                    Manufacturer = "Merck",
                    Category = Category.Other,
                    Indication = "diabetes",
                    ShortInfo = "Januvia (sitagliptin) is an oral diabetes medicine that helps control blood sugar levels. It works by regulating the " +
                        "levels of insulin your body produces after eating. Januvia is for treating people with type 2 diabetes. Januvia is sometimes used " +
                        "in combination with other diabetes medications, but is not for treating type 1 diabetes.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Mobic",
                    Drug = "meloxicam",
                    Manufacturer = "Boehringer Ingelheim",
                    Category = Category.AntiInflammatory,
                    Indication = "anti-inflammatory",
                    ShortInfo = "Meloxicam is a nonsteroidal anti-inflammatory drug (NSAID). It works by reducing hormones that cause " +
                        "inflammation and pain in the body. Meloxicam is used to treat pain or inflammation caused by rheumatoid arthritis " +
                        "and osteoarthritis in adults. Meloxicam is also used to treat juvenile rheumatoid arthritis in children who are at " +
                        "least 2 years old.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Ritalin",
                    Drug = "methylphenidate",
                    Manufacturer = "Novartis",
                    Category = Category.Other,
                    Indication = "ADHD",
                    ShortInfo = "Ritalin(methylphenidate) is a central nervous system stimulant. It affects chemicals in the brain and nerves " +
                        "that contribute to hyperactivity and impulse control.Ritalin is used to treat attention deficit disorder(ADD), attention deficit " +
                        "hyperactivity disorder(ADHD), and narcolepsy.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Elocon",
                    Drug = "mometasone",
                    Manufacturer = "Merck",
                    Category = Category.AntiInflammatory,
                    Indication = "anti-inflammatory",
                    ShortInfo = "Elocon(mometasone) is a topical steroid. It reduces the actions of chemicals in the body that cause inflammation. " +
                        "Elocon is used to treat the inflammation and itching caused by a number of skin conditions such as allergic reactions, eczema, and psoriasis.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Biaxin",
                    Drug = "clarithromycin",
                    Manufacturer = "AbbVie Inc.",
                    Category = Category.Other,
                    Indication = "stomach ulcers",
                    ShortInfo = "Biaxin(clarithromycin) is a macrolide antibiotic. Clarithromycin fights bacteria in your body. Biaxin is used to " +
                        "treat many different types of bacterial infections affecting the skin and respiratory system.It is also used together with other medicines " +
                        "to treat stomach ulcers caused by Helicobacter pylori.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Ibuprofen",
                    Drug = "Ibuprofen",
                    Manufacturer = "Reckitt Benckiser",
                    Category = Category.PainReliever,
                    Indication = "flux symptoms",
                    ShortInfo = "Ibuprofen is a nonsteroidal anti - inflammatory drug(NSAID). It works by reducing hormones that cause inflammation " +
                        "and pain in the body. Ibuprofen is used to reduce fever and treat pain or inflammation caused by many conditions such as headache, " +
                        "toothache back pain arthritis menstrual cramps or minor injury.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Forteo",
                    Drug = "Teriparatid",
                    Manufacturer = "Eli Lilly and Co.",
                    Indication = "osteoporosis",
                    Category = Category.Other,
                    ShortInfo = "Forteo is a man - made form of a hormone called parathyroid that exists naturally in the body. " +
                        "Forteo increases bone density and increases bone strength to help prevent fractures. Forteo is used to treat " +
                        "osteoporosis in men and women who have a high risk of bone fracture.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Imitrex",
                    Drug = "sumatriptan",
                    Manufacturer = "GlaxoSmithKline",
                    Category = Category.PainReliever,
                    Indication = "migraine",
                    ShortInfo = "Imitrex(sumatriptan) is a headache medicine that narrows blood vessels around the brain. " +
                        "Sumatriptan also reduces substances in the body that can trigger headache pain, nausea, sensitivity to " +
                        "light and sound, and other migraine symptoms. Imitrex is used to treat migraine headaches. Imitrex will only " +
                        "treat a headache that has already begun. It will not prevent headaches or reduce the number of attacks. Imitrex " +
                        "should not be used to treat a common tension headache or a headache that causes loss of movement on one " +
                        "side of your body .Use this medicine only if your condition has been confirmed by a doctor as migraine headaches.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),

                },
                new Medicine()
                {

                    Name = "Proscar",
                    Drug = "finasterid",
                    Manufacturer = "Merck",
                    Category = Category.Other,
                    Indication = "BPH",
                    ShortInfo = "Proscar prevents the conversion of testosterone to dihydrotestosterone(DHT) in the body. DHT is involved " +
                        "in the development of benign prostatic hyperplasia(BPH).  Proscar is used to treat symptoms of benign prostatic " +
                        "hyperplasia(BPH) in men with an enlarged prostate.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                },
                new Medicine()
                {

                    Name = "Macrobid",
                    Drug = "nitrofurantoin",
                    Manufacturer = "Almatica Pharma",
                    Category = Category.AntiBiotic,
                    Indication = "antibiotic",
                    ShortInfo = "Macrobid (nitrofurantoin) is an antibiotic that fights bacteria in the body. Macrobid is used to treat urinary tract infections.",
                    Price = GenerateDecimal(8, 25),
                    InStockQty = GenerateInteger(0, 250),
                }};

            medicineList.ForEach(d => context.Medicines.AddOrUpdate(x => x.Name, d));
            SaveToDb(context);
        }
        #endregion

        private void SeedCreditCards(ApplicationDbContext context)
        {

            /*Get credit card numbers from
              http://www.getcreditcardnumbers.com/ 
            */

            var marta = context.Users.First(d => d.NameIdentifier == "Marta Pancaldi");
            var werner = context.Users.First(d => d.NameIdentifier == "Werner Sperandio");
            var tahir = context.Users.First(d => d.NameIdentifier == "Tahir Karim");
            var sven = context.Users.First(d => d.NameIdentifier == "Sven Helmer");

            ccList = new List<CreditCard>() { 
            #region Marta

                new CreditCard()
                {

                    CardNumber = "4539967009649320",
                    ExpireMonth = 12,
                    ExpireYear = 2018,
                    Customer = marta,
                    FirstName = "Marta",
                    LastName = "Pancaldi"
                },
                new CreditCard()
                {

                    CardNumber = "5456055244892362",
                    ExpireMonth = 09,
                    ExpireYear = 2022,
                    Customer = marta,
                    FirstName = "Marta",
                    LastName = "Pancaldi"
                },
                new CreditCard()
                {

                    CardNumber = "6011164757536261",
                    ExpireMonth = 8,
                    ExpireYear = 2018,
                    Customer = marta,
                    FirstName = "Marta",
                    LastName = "Pancaldi"
                },
                new CreditCard()
                {

                    CardNumber = "5207071845167445",
                    ExpireMonth = 10,
                    ExpireYear = 2022,
                    Customer = marta,
                    FirstName = "Marta",
                    LastName = "Pancaldi"
                },

            #endregion
            #region Werner

                new CreditCard()
                {
                    CardNumber = "346018109844626",
                    ExpireMonth = 07,
                    ExpireYear = 2019,
                    Customer = werner,
                    FirstName = "Werner",
                    LastName = "Sperandio"
                },
                new CreditCard()
                {
                    CardNumber = "5205451655463187",
                    ExpireMonth = 05,
                    ExpireYear = 2022,
                    Customer = werner,
                    FirstName = "Werner",
                    LastName = "Sperandio"
                },
                new CreditCard()
                {
                    CardNumber = "5523698466866884",
                    ExpireMonth = 12,
                    ExpireYear = 2017,
                    Customer = werner,
                    FirstName = "Werner",
                    LastName = "Sperandio"
                },
                new CreditCard()
                {
                    CardNumber = "4024007165758540",
                    ExpireMonth = 04,
                    ExpireYear = 2025,
                    Customer = werner,
                    FirstName = "Werner",
                    LastName = "Sperandio"
                },
                #endregion
            #region Tahir

                new CreditCard()
                {
                    CardNumber = "5237260849197128",
                    ExpireMonth = 07,
                    ExpireYear = 2019,
                    Customer = tahir,
                    FirstName = "Tahir",
                    LastName = "Karim"
                },
                new CreditCard()
                {
                    CardNumber = "4929096603475417",
                    ExpireMonth = 05,
                    ExpireYear = 2022,
                    Customer = tahir,
                    FirstName = "Tahir",
                    LastName = "Karim"
                },
                new CreditCard()
                {
                    CardNumber = "375998341561316",
                    ExpireMonth = 12,
                    ExpireYear = 2017,
                    Customer = tahir,
                    FirstName = "Tahir",
                    LastName = "Karim"
                },
                #endregion
            #region sven

                new CreditCard()
                {
                    CardNumber = "371621666864545",
                    ExpireMonth = 06,
                    ExpireYear = 2025,
                    Customer = sven,
                    FirstName = "Sven",
                    LastName = "Helmer"
                },
                #endregion

        };
            context.CreditCards.AddOrUpdate(x => x.CustomerId, ccList.ToArray());
            SaveToDb(context);
        }

        private decimal GenerateDecimal(int min, int max)
        {
            return (decimal)(Rng.NextDouble() * max + min);
        }

        private int GenerateInteger(int min, int max)
        {
            return Rng.Next(min, max);
        }

        private void SeedShippingAdresses(ApplicationDbContext context)
        {
            var marta = context.Users.First(d => d.NameIdentifier == "Marta Pancaldi");
            var werner = context.Users.First(d => d.NameIdentifier == "Werner Sperandio");
            var tahir = context.Users.First(d => d.NameIdentifier == "Tahir Karim");
            var sven = context.Users.First(d => d.NameIdentifier == "Sven Helmer");
            var paolo = context.Users.First(d => d.NameIdentifier == "Paolo Bolzoni");

            addrList = new List<ShippingAddress>() { 
            #region Marta

                new ShippingAddress()
                {
                    Street = "Rechtentalstr. 49",
                    City ="Spechtenhausen",
                    State="Germany",
                    ZIP="02105",
                    Customer = marta
                },
                new ShippingAddress()
                {
                    Street = "Reichstr. 17a",
                    City ="Muenchen",
                    State="Germany",
                    ZIP="4910",
                    Customer = marta
                },
            #endregion
            #region Werner

                new ShippingAddress()
                {
                    Street = "Lanerweg 12",
                    City ="Lana",
                    State="Italy",
                    ZIP="39055",
                    Customer = werner
                },
                new ShippingAddress()
                {
                    Street = "Wendelsteinstrasse 45",
                    City ="Bozen",
                    State="Italy",
                    ZIP="39100",
                    Customer = werner
                },
                new ShippingAddress()
                {
                    Street = "Waltherplatz 1b",
                    City ="Bozen",
                    State="Italy",
                    ZIP="39100",
                    Customer = werner
                },
                new ShippingAddress()
                {
                    Street = "Heinrichstr.",
                    City ="Kaltern",
                    State="Italy",
                    ZIP="39050",
                    Customer = werner
                },
                #endregion
            #region Tahir

                new ShippingAddress()
                {
                    Street = "A. Hoferstr. 1",
                    City ="Innbsruck",
                    State="Austria",
                    ZIP="4586",
                    Customer = tahir
                },
                #endregion
            #region sven

                new ShippingAddress()
                {
                    Street = "Am Spaechter 6",
                    City ="Nuernberg",
                    State="Germany",
                    ZIP="7585",
                    Customer = sven
                },
                new ShippingAddress()
                {
                    Street = "Reichsallee. 20",
                    City ="Berlin",
                    State="Germany",
                    ZIP="04510",
                    Customer = sven
                },
                #endregion
            #region Paolo

                new ShippingAddress()
                {
                    Street = "Corso Liberta 5",
                    City ="Roma",
                    State="Italy",
                    ZIP="02105",
                    Customer = paolo
                },
                new ShippingAddress()
                {
                    Street = "Corso Liberta 6",
                    City ="Roma",
                    State="Italy",
                    ZIP="02105",
                    Customer = paolo
                },
                new ShippingAddress()
                {
                    Street = "Piazza Mazzini 12",
                    City ="Milano",
                    State="Italy",
                    ZIP="02105",
                    Customer = paolo
                }
                #endregion
        };
            context.ShippingAddresses.AddOrUpdate(x => x.CreatedOn, addrList.ToArray());
            context.SaveChanges();
        }

        private void SeedReviews(ApplicationDbContext context)
        {
            // associate to Order itmes of Orders
            var u = context.Users.Where(usr => usr.NameIdentifier == "overlord").First();
            var m = medicineList.Find(med => med.Name == "Xanax");

            reviewList = new List<Review>() {
            new Review()
            {
                Comment = "Hello World",
                Stars = 2,
                Customer = u,
                Medicine = m
            } };
            context.Reviews.AddOrUpdate(x => x.Comment, reviewList.ToArray());
            SaveToDb(context);
        }

        private void GenerateOrderItems()
        {
            oderItemList = new List<OrderItem>();

            for (int i = 0; i < 200; i++)
            {
                var med = medicineList[GenerateInteger(0, medicineList.Count)];
                var qnt = GenerateInteger(1, 5);
                oderItemList.Add(
                    new OrderItem()
                    {
                        Medicine = med,
                        Quantity = qnt,
                        SubTotal = med.Price * qnt
                    }
                );

            };
        }

        private void SeedOrders(ApplicationDbContext context)
        {
            var marta = context.Users.First(d => d.NameIdentifier == "Marta Pancaldi");
            var werner = context.Users.First(d => d.NameIdentifier == "Werner Sperandio");
            var tahir = context.Users.First(d => d.NameIdentifier == "Tahir Karim");
            var sven = context.Users.First(d => d.NameIdentifier == "Sven Helmer");

            GenerateOrderItems();

            orderList = new List<Order>() { 
            #region Marta
                new Order()
                {
                    OrderItems = oderItemList.GetRange(0,4),
                    Status = OrderStatus.Submitted,
                    Address = marta.ShippingAddresses.ToList()[0],
                    CreditCard = marta.CreditCards.ToList()[0],
                    Customer = marta
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(5,8),
                    Status = OrderStatus.Submitted,
                    Address = marta.ShippingAddresses.ToList()[0],
                    CreditCard = marta.CreditCards.ToList()[0],
                    Customer = marta
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(9,15),
                    Status = OrderStatus.Submitted,
                    Address = marta.ShippingAddresses.ToList()[0],
                    CreditCard = marta.CreditCards.ToList()[0],
                    Customer = marta
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(16,20),
                    Status = OrderStatus.Submitted,
                    Address = marta.ShippingAddresses.ToList()[0],
                    CreditCard = marta.CreditCards.ToList()[0],
                    Customer = marta
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(21,22),
                    Status = OrderStatus.Submitted,
                    Address = marta.ShippingAddresses.ToList()[0],
                    CreditCard = marta.CreditCards.ToList()[0],
                    Customer = marta
                },
            #endregion
            #region Werner

                new Order()
                {
                    OrderItems = oderItemList.GetRange(23,30),
                    Status = OrderStatus.Submitted,
                    Address = werner.ShippingAddresses.ToList()[0],
                    CreditCard = werner.CreditCards.ToList()[0],
                    Customer = werner
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(31,35),
                    Status = OrderStatus.Submitted,
                    Address = werner.ShippingAddresses.ToList()[0],
                    CreditCard = werner.CreditCards.ToList()[0],
                    Customer = werner
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(36,38),
                    Status = OrderStatus.Submitted,
                    Address = werner.ShippingAddresses.ToList()[0],
                    CreditCard = werner.CreditCards.ToList()[0],
                    Customer = marta
                },
                #endregion
            #region Tahir

                new Order()
                {
                    OrderItems = oderItemList.GetRange(16,20),
                    Status = OrderStatus.Submitted,
                    Address = tahir.ShippingAddresses.ToList()[0],
                    CreditCard = tahir.CreditCards.ToList()[0],
                    Customer = tahir
                },
                new Order()
                {
                    OrderItems = oderItemList.GetRange(21,22),
                    Status = OrderStatus.Submitted,
                    Address = tahir.ShippingAddresses.ToList()[0],
                    CreditCard = tahir.CreditCards.ToList()[0],
                    Customer = tahir
                }
            #endregion
            };

            foreach (Order order in orderList)
            {
                var orderItems = order.OrderItems;
                decimal orderTotal = 0;
                foreach (OrderItem item in orderItems)
                {
                    orderTotal += item.SubTotal;
                }
                order.TotalPrice = orderTotal;
            }

            context.Orders.AddOrUpdate(x => x.CreatedOn, orderList.ToArray());
            SaveToDb(context);
        }

        /// <summary>
        /// Wrapper for SaveChanges adding the Validation Messages to the generated exception
        /// </summary>
        /// <param name="context">The context.</param>
        private void SaveToDb(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                StringBuilder sb = new StringBuilder();

                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }

                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                ); // Add the original exception as the innerException
            }
        }
    }
}
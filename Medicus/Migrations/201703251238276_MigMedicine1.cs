namespace Medicus.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigMedicine1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Medicines", "Drug", c => c.String(nullable: false));
            AddColumn("dbo.Medicines", "Indication", c => c.String());
            AddColumn("dbo.Medicines", "Company", c => c.String());
            AddColumn("dbo.Medicines", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Medicines", "InStockQty", c => c.Int(nullable: false));
            AlterColumn("dbo.Medicines", "Name", c => c.String(nullable: false));
            DropColumn("dbo.Medicines", "Ingredients");
            DropColumn("dbo.Medicines", "ActiveAgent");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Medicines", "ActiveAgent", c => c.String());
            AddColumn("dbo.Medicines", "Ingredients", c => c.String());
            AlterColumn("dbo.Medicines", "Name", c => c.String());
            DropColumn("dbo.Medicines", "InStockQty");
            DropColumn("dbo.Medicines", "Price");
            DropColumn("dbo.Medicines", "Company");
            DropColumn("dbo.Medicines", "Indication");
            DropColumn("dbo.Medicines", "Drug");
        }
    }
}

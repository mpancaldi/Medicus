﻿/**
 * This js file collects a number of useful functions for
 * the bootstrap table plugin, like column formatters and
 * sorters.
 *
 */

/**
 * Function formating a boolean column.
 * 
 * @param {Object}
 *            value The row value.
 * @param {Object}
 *			  row The row object.
 */
function boolFormatter(value, row) {
    var icon = value === true ? 'fa-check' : 'fa-times';
    return '<i class="fa ' + icon + '"></i> ';
}

/**
 * Function for sorting rows based on boolean column.
 * 
 * @param {boolean}
 *            a The first value.
 * @param {boolean}
 *			  b The value to compared with.
 */
function boolSorter(a, b) {
    return (a === b) ? 0 : a ? -1 : 1;
}

/**
 * Formats the action column of the table by adding an edit, details and delete icon.
 * 
 * @param {number}
 *            value The row number.
 */
function actionFormatter(value, row) {

    var controller = $('#bs-table').data('controller');
    return [
        '<a class="edit" href="/Admin/' + controller + '/Edit/' + row.Id + '"title="Edit"><i class="fa fa-pencil"></i>  </a>' + 
        '<a class="edit" href="/Admin/' + controller + '/Details/' + row.Id + '"title="Details"><i class="fa fa-info-circle"></i>  </a>' +
        '<a class="edit" href="/Admin/' + controller + '/Delete/' + row.Id + '"title="Delete"><i class="fa fa-trash"></i>  </a>' ]
        .join('');
}

function orderActionFormatter(value, row) {

    return [
        '<a class="edit" href="/Admin/Orders/Edit/' + row.Id + '"title="Edit"><i class="fa fa-pencil"></i>  </a>' +
        '<a class="edit" href="/Admin/Orders/Details/' + row.Id + '"title="Details"><i class="fa fa-info-circle"></i>  </a>' +
        '<a class="edit" href="/Admin/Orders/Delete/' + row.Id + '"title="Delete"><i class="fa fa-trash"></i>  </a>']
        .join('');
}


//function nameFormatter(value, row) {
//    var icon = row.id % 2 === 0 ? 'glyphicon-star' : 'glyphicon-star-empty'
//    return '<i class="glyphicon ' + icon + '"></i> ' + value;
//}


/**
 * Formats the table category column of the table by adding star icons.
 * 
 * @param {number}
 *            value The row number.
 */
function categoryFormatter(value) {
    var retvalue = '';
    switch (value) {

        case 'lowcost':
            retvalue = '<i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>';
            break;

        case 'medium':
            retvalue = '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i>';
            break;

        case 'highend':
            retvalue = '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
            break;
    }

    return retvalue;
}

/**
* Formats the price column of the table by adding a Euro sign.
* 
* @param {number}
*            value The row number.
*/
function priceFormatter(value) {
    var num = parseFloat(value);
    return '€ ' + num.toFixed(2);
}

/**
 * Returns the number of selected rows (its id's).
 * 
 * @return {number}
 *            value The selected row numbers.
 */
function getIdSelections() {
    return $.map($table.bootstrapTable('getSelections'), function (row) {
        return row.id;
    });
}
﻿
/*
* Created by Marta Pancaldi on 10/4/2017
*/

$(function () {

    // Increase quantity by one
    $(".add-link").click(function () {
        var recordToAdd = $(this).attr("data-id");
        if (recordToAdd != '') {
            $.post("/ShoppingCart/AddOneToCart", { "id": recordToAdd },
                function (data) {
                    $('#item-count-' + data.DeleteId).text(data.ItemCount);
                    $('.cart-total').text('$' + data.CartTotal.toFixed(2));
                    $('#cart-status').text('Cart (' + data.CartCount + ')');
                    $('#sub-total-' + data.DeleteId).text('$' + data.ItemSubTotal.toFixed(2));
                    $('#cart-summary').load("/ShoppingCart/Summary");
                });
        }
    });

    // Decrease quantity one by one
    $(".remove-link").click(function () {
        var recordToDelete = $(this).attr("data-id");
        if (recordToDelete != '') {
            $.post("/ShoppingCart/RemoveOneFromCart", { "id": recordToDelete },
                function (data) {
                    if (data.ItemCount == 0) {
                        $('#row-' + data.DeleteId).fadeOut('slow');
                    } else {
                        $('#item-count-' + data.DeleteId).text(data.ItemCount);
                    }
                    $('.cart-total').text('$' + data.CartTotal.toFixed(2));
                    $('#cart-status').text('Cart (' + data.CartCount + ')');
                    $('#sub-total-' + data.DeleteId).text('$' + data.ItemSubTotal.toFixed(2));
                    $('#cart-summary').load("/ShoppingCart/Summary");
                });
        }
    });
    
    //remove item (= set quantity to 0)
    $(".remove-item-link").click(function () {
        var recordToDelete = $(this).attr("data-id");
        if (recordToDelete != '') {
            $.post("/ShoppingCart/RemoveFromCart", { "id": recordToDelete },
                function (data) {
                    $('#row-' + data.DeleteId).fadeOut('slow');
                    $('.cart-total').text('$' + data.CartTotal.toFixed(2));
                    $('#msg-text').text(data.Message);
                    $('#update-message').fadeTo(3000, 500).slideUp(1000, function () {
                        $("#update-message").slideUp(500);
                    });   
                    $('#cart-status').text('Cart (' + data.CartCount + ')');
                    $('#sub-total-' + data.DeleteId).text('$' + data.ItemSubTotal.toFixed(2));
                    $('#cart-summary').load("/ShoppingCart/Summary");
                });
        }
    });
});

function handleUpdate() {
    // Load and deserialize the returned JSON data
    var json = context.get_data();
    var data = Sys.Serialization.JavaScriptSerializer.deserialize(json);

    if (data.ItemCount == 0) {
        $('#row-' + data.DeleteId).fadeOut('slow');
    } else {
        $('#item-count-' + data.DeleteId).text(data.ItemCount);
    }

    $('#cart-total').text('$' + data.CartTotal);
    $('#update-message').text(data.Message);
    $('#cart-status').text('Cart (' + data.CartCount + ')');
    $('#sub-total-' + data.DeleteId).text('$' + data.ItemSubTotal.toFixed(2));
}
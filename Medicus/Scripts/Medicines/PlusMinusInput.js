﻿$(document).ready(function () {

    var quantity = 0;

    $('.quantity-right-plus').click(function (e) {
        e.preventDefault();
        var quantity = parseInt($('#quantity').val());
        if (quantity < 20)
            $('#quantity').val(quantity + 1);

    });

    $('.quantity-left-minus').click(function (e) {    
        e.preventDefault();
        var quantity = parseInt($('#quantity').val());
        if (quantity > 1) {
            $('#quantity').val(quantity - 1);
        }
    });

    $('#reset-count').click(function () {
        $('#quantity').val(1);   
    });

    $("#add-to-cart").click(function () {
        var quantity = $('#quantity').val(); 
        var medId = $(this).attr("data-id");
        window.location.href = '/ShoppingCart/AddToCart/' + medId + '/?qnt=' + quantity;
    });
});
﻿$(function(e) {
    $('.search-panel .dropdown-menu').find('a').click(function (e) {
        e.preventDefault();
        var filtervalue = $(this).attr("value");
        var filtername = $(this).text();
        $('.search-panel #filter-button span').first().text(filtername);
        $('.input-group #filter-category').val(filtervalue);
        $('form').submit();
    });
});
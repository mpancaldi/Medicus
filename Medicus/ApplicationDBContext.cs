﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Medicus.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            AddTimestamps();
            return await base.SaveChangesAsync();
        }

        private void AddTimestamps()
        {
            var modEntities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Modified);
            var addEntities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && x.State == EntityState.Added);

            foreach (var entity in modEntities)
            {
                ((BaseEntity)entity.Entity).LastModified = DateTime.UtcNow;
            }

            foreach (var entity in addEntities)
            {
                ((BaseEntity)entity.Entity).CreatedOn = DateTime.UtcNow;
                ((BaseEntity)entity.Entity).LastModified = DateTime.UtcNow;

            }

        }


        //Here we have the Model Db Contexts
        public DbSet<Medicine> Medicines { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<Cart> Carts { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ShippingAddress> ShippingAddresses { get; set; }
        public DbSet<Review> Reviews { get; set; }
    }
}
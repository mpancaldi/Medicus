﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medicus.Models
{
    public class Order : BaseEntity
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Required]
        public string CustomerId { get; set; }

        //[Required]
        //public int AddressId { get; set; }

        //[Required]
        //public int CreditCardId { get; set; }

        [Display(Name = "Fulfilled Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}", ApplyFormatInEditMode = true)]
        public DateTime? FullFilledDate { get; set; }

        [Required]
        public OrderStatus Status { get; set; }

        [Display(Name = "Total Price")]
        [DisplayFormat(DataFormatString = "€ {0:N2}")]
        public decimal TotalPrice { get; set; }

        [Display(Name = "Credit Card")]
        public virtual CreditCard CreditCard { get; set; }

        [Display(Name = "Shipping Address")]
        public virtual ShippingAddress Address { get; set; }

        [Display(Name = "Order Items")]
        public virtual ICollection<OrderItem> OrderItems { get; set; }

        public virtual ApplicationUser Customer { get; set; }

        public void UpdateOrders()
        {
            ICollection<Order> orders = db.Orders.ToList();
            DateTime now = DateTime.UtcNow;
            foreach (Order order in orders)
            {
                long timePassed = (long)(now - order.CreatedOn).TotalMinutes;
                if (timePassed >= 15 && timePassed < 2 * 60)            order.Status = OrderStatus.Submitted;   //Set Submitted after 15'
                if (timePassed >= 2 * 60  && timePassed < 48 * 60)      order.Status = OrderStatus.Shipped;     //Set Shipped after 2 hours
                if (timePassed >= 48 * 60 && timePassed < 24 * 7 * 60)  order.Status = OrderStatus.Delivered;   //Set Delivered after 2 days
                if (timePassed >= 24 * 7 * 60)                          order.Status = OrderStatus.Completed;   //Set Completed after 7 days
                if (order.Status == OrderStatus.Completed) order.FullFilledDate = order.CreatedOn + new TimeSpan(7, 0, 0, 0); //Set Fullfilled date after 7 days
            }
            db.SaveChanges();
        }
    }

    public enum OrderStatus
    {
        [Display(Name = "Received")]
        Received,
        [Display(Name = "Submitted")]
        Submitted,
        [Display(Name = "Shipped")]
        Shipped,
        [Display(Name = "Delivered")]
        Delivered,
        [Display(Name = "Completed")]
        Completed
    }

}
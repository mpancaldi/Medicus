﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Data.Entity;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Core.Objects;

namespace Medicus.Models
{
    public class CustomIdentityUser : IdentityUser
    {
        // You can add profile data for the user by adding more properties to your CustomIdentityUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
        [Display(Name = "Name Identifier")]
        public string NameIdentifier { get; set; }

        [Display(Name = "Credit Cards")]
        [InverseProperty("Customer")]
        public virtual ICollection<CreditCard> CreditCards { get; set; }

        [Display(Name = "Default Credit Card")]
        public virtual CreditCard DefaultCreditCard { get; set; }

        [Display(Name = "Shipping Addresses")]
        [InverseProperty("Customer")]
        public virtual ICollection<ShippingAddress> ShippingAddresses { get; set; }

        [Display(Name = "Default Shipping Address")]
        public virtual ShippingAddress DefaultAddress { get; set; }

        [InverseProperty("Customer")]
        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }

    }


    public class ApplicationUser : CustomIdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

            userIdentity.AddClaim(new Claim("Medicus.Models.RegisterViewModel.NameIdentifier", NameIdentifier));
            userIdentity.AddClaim(new Claim("Medicus.Models.RegisterViewModel.Email", Email));

            return userIdentity;
        }

        public static explicit operator ObjectQuery(ApplicationUser v)
        {
            throw new NotImplementedException();
        }
    }

}
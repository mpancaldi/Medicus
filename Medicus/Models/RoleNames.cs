﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medicus.Models
{
    public class RoleNames
    {
        public const string ADMINISTRATOR = "Administrator";
        public const string MANAGER = "Manager";
        public const string CUSTOMER = "Customer";
    }
}
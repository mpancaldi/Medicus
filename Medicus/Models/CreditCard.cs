﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Security;
using Medicus.Models;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Medicus
{
    public class CreditCard : BaseEntity
    {
        [Required]
        public string CustomerId { get; set; }

        [Required, CreditCard]
        [Display(Name = "Card Number")]
        public string CardNumber { get; set; }

        [Required]
        [Display(Name = "Expiration Month")]
        [Range(1,12)]
        public int ExpireMonth { get; set; }

        [Required]
        [Display(Name = "Expiration Year")]
        [Range(2015, 2100)]
        public int ExpireYear { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "The First Name field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [StringLength(50, ErrorMessage = "The Last Name field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public string LastName { get; set; }

        public virtual ApplicationUser Customer { get; set; }

    }
}
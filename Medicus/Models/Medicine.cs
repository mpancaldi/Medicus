﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using Medicus.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace Medicus
{
    public class Medicine : BaseEntity
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Drug { get; set; }
        [Required]
        public Category Category { get; set; }
        public string ShortInfo { get; set; }
        public string Description { get; set; }
        public string Indication { get; set; }
        public string Manufacturer { get; set; }
        [Required]
        public decimal Price { get; set; }
        [Required]
        public int InStockQty { get; set; }

        public virtual ICollection<Review> Reviews { get; set; }
    }

    public enum Category
    {
        [Display(Name = "Antibiotic")]
        AntiBiotic,
        [Display(Name = "Anti-inflammatory")]
        AntiInflammatory,
        [Display(Name = "Steroid")]
        Steroid,
        [Display(Name = "Anti-hypertensive")]
        AntiHypertensive,
        [Display(Name = "Pain reliever")]
        PainReliever,
        [Display(Name = "Cholesterol reducer")]
        CholesterolReducer,
        [Display(Name = "Anti-depressant")]
        AntiDepressant,
        [Display(Name = "Other")]
        Other

    }
}

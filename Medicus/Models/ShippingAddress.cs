﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel.DataAnnotations;
using Medicus.Models;
using System.Collections.Generic;

namespace Medicus
{
    public class ShippingAddress : BaseEntity
    {
        [Required]
        public string CustomerId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Street field must be between {2} and {1} characters long.", MinimumLength = 5)]
        public string Street { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "The City field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public string City { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The Country field must be between {2} and {1} characters long.", MinimumLength = 5)]
        public string State { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The ZIP field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public string ZIP { get; set; }

        public virtual ApplicationUser Customer { get; set; }

    }
}
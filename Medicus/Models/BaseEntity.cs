﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Medicus.Models
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Created On")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime CreatedOn { get; set; }

        [Column(TypeName = "datetime2")]
        [Display(Name = "Last Modified")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime LastModified { get; set; }

        [Timestamp]
        public byte[] RowID { get; set; }
    }
}
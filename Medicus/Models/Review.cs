﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medicus.Models
{
    public class Review : BaseEntity
    {
        [Required]
        public string CustomerId { get; set; }

        [Required]
        public int MedicineId { get; set; }

        [Range(1, 5)]
        [Display(Name = "Rating")]
        [Required]
        public int Stars { get; set; }

        [Required]
        public string Comment { get; set; }

        public virtual ApplicationUser Customer { get; set; }

        public virtual Medicine Medicine { get; set; }

    }
}
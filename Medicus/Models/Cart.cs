﻿using System.ComponentModel.DataAnnotations;

namespace Medicus.Models
{
    public class Cart : BaseEntity
    {
        public string CartId { get; set; }
        public int MedicineId { get; set; }
        public int Count { get; set; }

        public virtual Medicine Medicine { get; set; }
    }
}
﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Collections.Generic;
using Medicus.Models;
using System.ComponentModel.DataAnnotations;

namespace Medicus
{
    public class OrderItem : BaseEntity
    {
        [Required]
        public int OrderId { get; set; }

        [Required]
        public int MedicineId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [DisplayFormat(DataFormatString = "€ {0:N2}")]
        public decimal SubTotal { get; set; }

        public virtual Order Order { get; set; }
        public virtual Medicine Medicine { get; set; }
    }
}
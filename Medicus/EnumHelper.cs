﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Medicus
{
    public static class EnumHelper
    {
        /// <summary>
        /// Gets an attribute on an enum field value
        /// </summary>
        /// <typeparam name="T">The type of the attribute to be retrieved</typeparam>
        /// <param name="enumVal">The enum value</param>
        /// <returns>The attribute of type T that exists on the enum value</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            Type type = enumVal.GetType();
            if (!type.IsEnum)
            {
                throw new ArgumentException("EnumerationValue must be of Enum type", "enumerationValue");
            }

            //Tries to find a attribute
            MemberInfo[] memberInfo = type.GetMember(enumVal.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);

                if (attributes != null && attributes.Length > 0)
                {
                    //Pull out the attribute
                    return ((T)attributes[0]);
                }
            }

            //return null if attribute not found
            return null;
        }


        /// <summary>
        /// Converts an enumeration into a key-value list, where the key are name friendly values and
        /// values are just the enumeration values
        /// </summary>
        /// <typeparam name="T">The type of the attribute you want to retrieve</typeparam>
        /// <param name="enumeration">The enumeration to be converted</param>
        /// <returns>A key value list</returns>
        /// <example>string desc = myEnumVariable.GetAttributeOfType<DescriptionAttribute>().Description;</example>
        public static List<KeyValuePair<string, int>> GetEnumlist(Enum enumeration)
        {
            var list = new List<KeyValuePair<string, int>>();

            foreach (Enum e in Enum.GetValues(enumeration.GetType()))
            {
                var descr = EnumHelper.GetAttributeOfType<DisplayAttribute>(e).Name;
                descr = (descr == null) ? e.ToString() : descr;
                list.Add(new KeyValuePair<string, int>(descr, Convert.ToInt32(e)));
            }

            return list;
        }
    }
}
﻿using System.Collections.Generic;
using Medicus.Models;

namespace Medicus.ViewModels
{
    public class ShoppingCartViewModel
    {
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
        public decimal SubTotal(Cart cartItem)
        {
            { return cartItem.Medicine.Price * cartItem.Count; }
        }
        public int CartItemCount { get; set; }
    }
}
﻿using System.Collections.Generic;
using Medicus.Models;
using Medicus.ViewModels;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Medicus.ViewModels
{
    public class OrderConfirmViewModel
    {
        [Key]
        public int Id { get; set; }
        public ICollection<CreditCard> CreditCards { get; set; }
        public ICollection<ShippingAddress> ShippingAddresses { get; set; }
        public ShoppingCartViewModel ShoppingCart { get; set; }
        public bool AtLeastOneCard { get; set; }
        public bool AtLeastOneAddress { get; set; }
    }
}
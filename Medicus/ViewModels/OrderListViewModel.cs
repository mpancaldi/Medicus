﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Medicus.Models;
using System.ComponentModel.DataAnnotations;


namespace Medicus.ViewModels
{
    public class OrderListViewModel
    {
        private Order order;

        // ApplicationDbContext db = new ApplicationDbContext();

        public OrderListViewModel(Order order)
        {
            this.order = order;
        }

        public int Id { get { return order.Id; } }

        [Display(Name = "Fulfilled Date")]
        public string FullfilledDate
        {
            get
            {
                return order.FullFilledDate.ToString();
            }
        }

        [Display(Name = "Customer")]
        //public string CustomerName { get { return order.Customer.UserName; } }
        public string CustomerName { get { return order.Customer.NameIdentifier; } } 

        [Display(Name = "Total Price")]
        public string TotalPrice { get { return String.Format("€ {0:N2}", order.TotalPrice); } }

        [Display(Name = "Status")]
        public string Status { get
            {
                return EnumHelper.GetAttributeOfType<DisplayAttribute>(order.Status).Name;
            }
        }

        [Display(Name = "Address")]
        public String Address { get { return order.Address.Street + " " + order.Address.City + ", " + order.Address.ZIP + " " + order.Address.State; } }

        [Display(Name = "Created On")]
        public string CreatedOn { get { return order.CreatedOn.ToString(); } }

        [Display(Name = "Last Modified On")]
        public DateTime LastModified { get { return order.LastModified; } }
    }
}
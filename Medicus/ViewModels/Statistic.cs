﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Medicus.ViewModels
{
    public class Statistic
    {
        public int Orders { get; set; }
        public int Users { get; set; }
        public int Reviews { get; set; }
        public int Medicines { get; set; }
    }
}
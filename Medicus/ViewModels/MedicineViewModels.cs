﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medicus.Models
{
    public class MedicineViewModel

    {
        private Medicine medicine;

        public MedicineViewModel(Medicine medicine)
        {
            this.medicine = medicine;
        }

        public int Id { get { return medicine.Id; } }
        public string Name { get { return medicine.Name; } }
        public string Drug { get { return medicine.Drug; } }
        public string Description { get { return medicine.Description; } }
        public string Indication { get { return medicine.Indication; } }
        public string Manufacturer { get { return medicine.Manufacturer; } }
        public string ShortInfo { get { return medicine.ShortInfo; } }
        [Display(Name = "In Stock Qty")]
        public int InStockQty { get { return medicine.InStockQty; } }

        [Display(Name = "Category")]
        public string CategoryString
        {

            get
            {
                return EnumHelper.GetAttributeOfType<DisplayAttribute>(medicine.Category).Name;
            }

        }

        [Display(Name = "Price")]
        public string PriceString
        {
            get { return String.Format("€ {0:N2}", medicine.Price ); }

        }

        [Display(Name = "Short Information")]
        public string ShortInfoString
        {
            get
            {
                string info = medicine.ShortInfo;
                if (info != null)
                {
                    int last_pos;

                    if (info.Length > 120)
                    {
                        last_pos = info.LastIndexOf(" ", 120);
                        info = (last_pos == -1) ? info.Substring(0, 120) : info.Substring(0, last_pos);
                        info = info + " ...";
                    }
                }
                return info;

            }
        }
    }
}
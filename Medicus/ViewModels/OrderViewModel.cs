﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Medicus.Models
{
    public class OrderViewModel
    {
        private Order order;

        public OrderViewModel(Order order)
        {
            this.order = order;
        }

        public int Id { get { return order.Id; } }

        [Display(Name = "Fulfilled Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime? FullfilledDate { get { return order.FullFilledDate;  } }

        [Display(Name = "Customer")]
        //public string CustomerName { get { return order.Customer.UserName; } } //prevent circular reference that Json doesn't like
        public string CustomerName { get; set; } 

        [Display(Name = "Total Price")]
        //[DisplayFormat(DataFormatString = "{0:N2}")]
        public string TotalPrice { get { return String.Format("€ {0:N2}", order.TotalPrice); } }

        [Display(Name = "Status")]
        public OrderStatus Status { get { return order.Status; } }

        [Display(Name = "Address")]
        public String Address { get { return order.Address.Street + ", " + order.Address.City + ", " + order.Address.ZIP + " " + order.Address.State; } }

        [Display(Name = "Order Items")]
        public ICollection<OrderItem> OrderItems { get { return order.OrderItems; } }

        [Display(Name = "Created On")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime CreatedOn { get { return order.CreatedOn.ToLocalTime(); } }

        [Display(Name = "Last Modified On")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy HH:mm:ss}")]
        public DateTime LastModified { get { return order.LastModified.ToLocalTime(); } }
    }
}
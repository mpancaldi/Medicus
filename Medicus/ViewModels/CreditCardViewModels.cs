﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medicus.ViewModels
{
    public class CreditCardViewModel
    {
        private const string NATURAL_NAME_REGEX = @"^[aA-zZ ]+$";

        [Required, CreditCard]
        [Display(Name = "Card Number")]
        public virtual string CardNumber { get; set; }

        [Required]
        [Display(Name = "Expiration Month")]
        [Range(1, 12)]
        public virtual int ExpireMonth { get; set; }

        [Required]
        [Display(Name = "Expiration Year")]
        [Range(2015, 2100)]
        public virtual int ExpireYear { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [RegularExpression(NATURAL_NAME_REGEX, ErrorMessage = "The First Name field field must include only letters and spaces")]
        [StringLength(50, ErrorMessage = "The First Name field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public virtual string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [RegularExpression(NATURAL_NAME_REGEX, ErrorMessage = "The Last Name field field must include only letters and spaces")]
        [StringLength(50, ErrorMessage = "The Last Name field must be between {2} and {1} characters long.", MinimumLength = 3)]
        public virtual string LastName { get; set; }
    }

    public class CreditCardShowViewModel : CreditCardViewModel
    {
        private CreditCard creditcard;

        public CreditCardShowViewModel()
        {
        }

        public CreditCardShowViewModel(CreditCard creditcard)
        {
            this.creditcard = creditcard;
        }

        public int Id { get { return creditcard.Id; } }

        public override string CardNumber
        {
            get
            {
                var toMask = creditcard.CardNumber.Substring(0, creditcard.CardNumber.Length - 4);
                return creditcard.CardNumber.Replace(toMask, "************");
            }
        }

        public override int ExpireMonth { get { return creditcard.ExpireMonth; } }

        public override int ExpireYear { get { return creditcard.ExpireYear; } }

        public override string FirstName { get { return creditcard.FirstName; } }

        public override string LastName { get { return creditcard.LastName; } }

    }

    public class CreditCardCreateViewModel : CreditCardViewModel
    {
        private readonly int[] months = Enumerable.Range(1, 12).ToArray();
        private readonly int[] years = Enumerable.Range(DateTime.Now.Year, 11).ToArray();

        public IEnumerable<SelectListItem> ExpireMonths { set; get; }
        public IEnumerable<SelectListItem> ExpireYears { set; get; }

        public CreditCardCreateViewModel()
        {
            ExpireMonths = new List<SelectListItem>();
            ExpireYears = new List<SelectListItem>();

            ExpireMonths = months.Select(x =>
                        new SelectListItem
                        {
                            Text = x.ToString(),
                            Value = x.ToString(),
                            Selected = (x == DateTime.Today.Month)
                        });

            ExpireYears = years.Select(x =>
                        new SelectListItem
                        {
                            Text = x.ToString(),
                            Value = x.ToString(),
                            Selected = (x == DateTime.Today.Year)
                        });
        }


    }

    public class CreditCardEditViewModel : CreditCardCreateViewModel
    {
        public byte[] RowID { get; set; }
    }

}
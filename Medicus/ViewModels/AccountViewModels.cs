﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Data.Entity;
using Mvc = System.Web.Mvc;

namespace Medicus.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Text)]
        [Display(Name = "User Name")]
        public string NameIdentifier { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class UserViewModel
    {
        private ApplicationUser usr;
        private UserStore<ApplicationUser> userStore;
        private UserManager<ApplicationUser> userManager;

        public UserViewModel(ApplicationUser usr)
        {
            this.usr = usr;
            userStore = new UserStore<ApplicationUser>(new ApplicationDbContext());
            userManager = new UserManager<ApplicationUser>(userStore);

        }

        public string Id { get { return usr.Id; } }

        [Display(Name = "User Name")]
        public string NameIdentifier { get { return usr.NameIdentifier; } }

        [EmailAddress]
        [Display(Name = "Email Address")]
        public string Email { get { return usr.Email; } }

        [Display(Name = "Roles")]
        public string Roles
        {
            get
            {
                var roles = userManager.GetRoles(usr.Id);
                return String.Join(", ", roles).ToString();
            }
        }

        [Display(Name = "Email Confirmation Status")]
        public bool EmailConfirmed { get { return usr.EmailConfirmed; } }

        [Display(Name = "Email Confirmed")]
        public string EmailConfirmedString { get { return ConfirmedString(usr.EmailConfirmed); } }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get { return usr.PhoneNumber; } }

        [Display(Name = "Phone Confirmed")]
        public bool PhoneNumberConfirmed { get { return usr.PhoneNumberConfirmed; } }

        [Display(Name = "Phone Confirmed")]
        public string PhoneNumberConfirmedString { get { return ConfirmedString(usr.PhoneNumberConfirmed); } }

        [Display(Name = "2FA Enabled")]
        public bool TwoFactorEnabled { get { return usr.TwoFactorEnabled; } }

        [Display(Name = "2FA Enabled")]
        public string TwoFactorEnabledString { get { return ConfirmedString(usr.TwoFactorEnabled); } }

        [Display(Name = "Lock out enabled")]
        public bool LockoutEnabled { get { return usr.LockoutEnabled; } }

        [Display(Name = "Locked out enabled")]
        public string LockoutEnabledString
        {
            get
            {
                return (usr.LockoutEnabled) ? "Yes" : "No";
            }
        }

        [Display(Name = "Locked out until")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public string LockOutEndDateUtc
        {
            get
            {
                return String.Format("{0:MM/dd/yy H:mm:ss zzz}", usr.LockoutEndDateUtc);
            }
        }

        [Display(Name = "Failed Accesses")]
        public int AccessFailedCount { get { return usr.AccessFailedCount; } }


        private string ConfirmedString(bool val)
        {
            return (val) ? "Confirmed" : "Not confirmed";
        }

    }

    public class UserCreateModel : RegisterViewModel
    {
        private readonly RoleManager<IdentityRole> roleManager;
        public List<Mvc.SelectListItem> Roles { set; get; }

        [Display(Name = "Role")]
        [Required]
        public string SelectedRole { set; get; }

        public UserCreateModel()
        {
            roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            var roles = roleManager.Roles.ToListAsync().Result;
            Roles = new List<Mvc.SelectListItem>();
            foreach (var r in roles)
            {
                Roles.Add(new Mvc.SelectListItem
                {
                    Text = r.Name,
                    Value = r.Name
                });
                }

        }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }


    }
}

﻿using Medicus.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Medicus.ViewModels
{
    public class ShippingAddressViewModel
    {
        private const string NAME_REGEX = @"^[aA-zZ0-9 ,.'()-]+$";
        private const string ZIP_REGEX = @"^[aA-zZ0-9 ]+$";

        [RegularExpression(NAME_REGEX, ErrorMessage = "The Street field must include only the following symbols: letters, numbers, , . ' - ( ).")]
        [StringLength(100, ErrorMessage = "The Street field must be between {2} and {1} characters long.", MinimumLength = 5)]
        [Required]
        public string Street { get; set; }

        [RegularExpression(NAME_REGEX, ErrorMessage = "The City field must include only the following symbols: letters, numbers, , . ' - ( ).")]
        [StringLength(30, ErrorMessage = "The City field must be between {2} and {1} characters long.", MinimumLength = 3)]
        [Required]
        public string City { get; set; }

        [Display(Name = "Country")]
        [RegularExpression(NAME_REGEX, ErrorMessage = "The Country field must include only the following symbols: letters, numbers, ' - ( ).")]
        [StringLength(100, ErrorMessage = "The Country field must be between {2} and {1} characters long.", MinimumLength = 5)]
        [Required]
        public string State { get; set; }

        [RegularExpression(ZIP_REGEX, ErrorMessage = "The ZIP field must include only letters, numbers and spaces.")]
        [StringLength(100, ErrorMessage = "The ZIP field must be between {2} and {1} characters long.", MinimumLength = 3)]
        [Required]
        public string ZIP { get; set; }

        public byte[] RowID { get; set; }
    }
}
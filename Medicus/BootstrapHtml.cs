﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using WebGrease.Css.Extensions;

namespace Medicus
{
    public class BootstrapHtml
    {
        public static MvcHtmlString DropdownButton(string id, Enum enumeration, string label)
        {
            var button = new TagBuilder("button")
            {
                Attributes =
            {
                {"id", id},
                {"type", "button"},
                {"data-toggle", "dropdown"}
            }
            };

            button.AddCssClass("btn");
            button.AddCssClass("btn-default");
            button.AddCssClass("dropdown-toggle");

            button.InnerHtml = BuildLabel(label);
            button.InnerHtml += " " + BuildCaret();

            var dropdown = BuildDropdown(id, EnumHelper.GetEnumlist(enumeration));
            
            return new MvcHtmlString(button.ToString() + dropdown.ToString());
        }

        private static string BuildCaret()
        {
            var caret = new TagBuilder("span");
            caret.AddCssClass("caret");

            return caret.ToString();
        }

        private static string BuildLabel(string label)
        {
            var buttonText = new TagBuilder("span");
            buttonText.SetInnerText(label);

            return buttonText.ToString();
        }

        private static string BuildDropdown(string id, List<KeyValuePair<string, int>> dict)
        {
            var list = new TagBuilder("ul")
            {
                Attributes =
            {
                {"class", "dropdown-menu"},
                {"role", "menu"},
                {"aria-labelledby", id}
            }
            };

            var listItem = new TagBuilder("li");
            listItem.Attributes.Add("role", "presentation");

            dict.ForEach(x => list.InnerHtml += "<li role=\"presentation\">" + BuildListRow(x) + "</li>");

            return list.ToString();
        }

        private static string BuildListRow(KeyValuePair<string, int> items)
        {
            var anchor = new TagBuilder("a")
            {
                Attributes =
            {
                {"role", "menuitem"},
                {"tabindex", "-1"},
                {"href", "#"},
                {"value", items.Value.ToString()}
            }
            };

            anchor.SetInnerText(items.Key);

            return anchor.ToString();
        }
    }
}
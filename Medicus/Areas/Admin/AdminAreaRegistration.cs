﻿using System.Web.Mvc;

namespace Medicus.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {

            //set specific routes before general route area
            context.MapRoute(
                name: "Medicines",
                url: "Admin/Medicines/{action}/{id}",
                defaults: new { controller = "Medicines", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Medicus.Areas.Admin.Controllers" }
            );

            context.MapRoute(
                name: "Users",
                url: "Admin/Users/{action}/{id}",
                defaults: new { controller = "Users", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "Medicus.Areas.Admin.Controllers" }
            );


            context.MapRoute(
                name: "Admin_default",
                url: "Admin/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Dashboard", id = UrlParameter.Optional },
                namespaces: new[] { "Medicus.Areas.Admin.Controllers" }
            );
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Medicus;
using Medicus.Models;
using System.Data.Entity.Infrastructure;

namespace Medicus.Areas.Admin.Controllers
{
    [AllowAnonymous]
    public class MedicinesController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Medicines
        public ActionResult Index()
        {
            return View(new MedicineViewModel(null));
        }


        [HttpPost]
        public JsonResult List()
        {

            List<MedicineViewModel> medicineVMList = new List<MedicineViewModel>();
            foreach (var medicine in db.Medicines)
            {
                medicineVMList.Add(new MedicineViewModel(medicine));
            }
            return Json(medicineVMList.OrderBy(s => s.Name));
        }


        // GET: Admin/Medicines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(new MedicineViewModel(medicine));
        }

        // GET: Admin/Medicines/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Medicines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Drug,Category,ShortInfo,Description,Indication,Manufacturer,Price,InStockQty,CreateDate,ModifiedDate,RowID")] Medicine medicine)
        {
            if (ModelState.IsValid)
            {
                db.Medicines.Add(medicine);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(medicine);
        }

        // GET: Admin/Medicines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(medicine);
        }



        // POST: Admin/Medicines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Drug,Category,ShortInfo,Description,Indication,Manufacturer,Price,InStockQty,CreateDate,ModifiedDate,RowID")] Medicine medicine, int? id, byte[] RowID)
        {
            
            var originalRowId = (from r in db.Medicines
                                 where r.Id == id
                                 select r.RowID).SingleOrDefault();

            //if medicine to be edited isn't in db anymore
            if (originalRowId == null)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. The record was deleted by another user.");
                return View(medicine);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Entry(medicine).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    ModelState.AddModelError(string.Empty, "The record you attempted to edit "
                    + "was modified by another user after you got the original value. The "
                    + "edit operation was canceled and the current values in the database "
                    + "have been displayed. If you still want to edit this record, click "
                    + "the Save button again. Otherwise click the Back to List button.");

                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }

            // set original rowId to model and send back to view 
            medicine.RowID = originalRowId;
            return View(medicine);
        }

        // GET: Admin/Medicines/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(new MedicineViewModel(medicine));
        }

        // POST: Admin/Medicines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Medicine medicine = db.Medicines.Find(id);
            db.Medicines.Remove(medicine);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}

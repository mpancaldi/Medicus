﻿using System.Diagnostics;
using System.Collections.Generic;
using System.Web.Mvc;
using Medicus.Models;
using Medicus.ViewModels;
using System.Linq;
using System.Net;

namespace Medicus.Areas.Admin.Controllers
{
    public class OrdersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Orders
        public ActionResult Index()
        {
            Order order = new Order();
            order.UpdateOrders();
            return View(new OrderViewModel(null));
        }

        [HttpPost]
        public JsonResult List()
        {
            List<OrderListViewModel> orderVMList = new List<OrderListViewModel>();

            foreach (var order in db.Orders)
            {
                orderVMList.Add(new OrderListViewModel(order));
            }
            return Json(orderVMList.OrderByDescending(ord => ord.CreatedOn));
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            OrderViewModel viewModel = new OrderViewModel(order);
            ICollection<OrderItem> items = viewModel.OrderItems;
            foreach (OrderItem item in items)
            {
                item.Medicine = db.Medicines.Single(m => m.Id == item.Medicine.Id);
            }
            return View(viewModel);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Order order = db.Orders.Find(id);
            if (order == null)
            {
                return HttpNotFound();
            }
            return View(new OrderViewModel(order));
        }
    }
}
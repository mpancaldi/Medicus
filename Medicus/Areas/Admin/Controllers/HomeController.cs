﻿using Medicus.Models;
using Medicus.ViewModels;
using System.Linq;
using System.Web.Mvc;

namespace Medicus.Areas.Admin.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Dashboard()
        {
            Statistic st = new Statistic();
            st.Orders = db.Orders.AsQueryable().Count();
            st.Users = db.Users.AsQueryable().Count();
            st.Reviews = db.Reviews.AsQueryable().Count();
            st.Medicines = db.Medicines.AsQueryable().Count();
            return View(st);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Medicus.Areas.Admin.Controllers
{

    public class TestController : Controller
    {
        // GET: Admin/Test
        public string Index()
        {
            return "This is the test controller for URL: Index of controller " + this.ControllerContext.RouteData.Values["controller"].ToString();
        }
    }
}

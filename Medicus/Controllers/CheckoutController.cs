﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medicus.Models;
using Medicus.ViewModels;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.Entity;

namespace Medicus.Controllers
{
    public class CheckoutController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Checkout/AddressAndPayment
        public ActionResult AddressAndPayment()
        {
            var usrId = User.Identity.GetUserId();
            ApplicationUser current = db.Users.Single(user => user.Id == usrId);
            var cart = ShoppingCart.GetCart(this.HttpContext);
            bool oneCard = false;
            bool oneAddress = false;
            ICollection<CreditCard> cards = current.CreditCards;
            if (cards.Count != 0 )
            {
                oneCard = true;
                //return RedirectToAction("Create", "Cards");
            }
            if (current.ShippingAddresses.Count != 0)
                oneAddress = true;
               // return RedirectToAction("Create", "Address");

            foreach (CreditCard c in cards)
            {
                MaskCard(c);
                Debug.WriteLine("card: " + c);
            }
            
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal(),
                CartItemCount = cart.GetCount()
            };

            var orderConfirmViewModel = new OrderConfirmViewModel
            {
                CreditCards = cards,
                ShippingAddresses = current.ShippingAddresses,
                ShoppingCart = viewModel,
                AtLeastOneCard = oneCard,
                AtLeastOneAddress = oneAddress
            };

            return View(orderConfirmViewModel);
        }

        // POST: /Checkout/AddressAndPayment
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddressAndPayment([Bind(Include = "Id,ShippingAddresses,CreditCards")]
            OrderConfirmViewModel model, int Id, int ShippingAddresses, int CreditCards)
        {
            Order order = new Order();
            var usrId = User.Identity.GetUserId();
            ApplicationUser current = db.Users.AsNoTracking().Single(user => user.Id == usrId);
            order.CustomerId = usrId;
            //order.Customer = current;
            //order.CreatedOn = DateTime.Now; //It triggered an error related to datetime2
            //order.LastModified = DateTime.Now;
            order.Status = OrderStatus.Received;
            int addressId = ShippingAddresses;
            int cardId = CreditCards;
            //order.Address = current.ShippingAddresses.Single(add => add.Id == addressId);
            order.Address = db.ShippingAddresses.Single(a => a.CustomerId == usrId && a.Id == addressId);
            //order.CreditCard = current.CreditCards.Single(card => card.Id == cardId);
            order.CreditCard = db.CreditCards.Single(c => c.CustomerId == usrId && c.Id == cardId);

            //Process the shopping cart
            var shoppingCart = ShoppingCart.GetCart(this.HttpContext);
            List<Cart> list = shoppingCart.GetCartItemsNoTracking();
            List<OrderItem> orderList = new List<OrderItem>();
            foreach (Cart c in list)
            {
                OrderItem item = new OrderItem()
                {
                    Medicine = c.Medicine,
                    SubTotal = c.Medicine.Price * c.Count,
                    Quantity = c.Count
                };
                orderList.Add(item);
            }
            order.TotalPrice = shoppingCart.GetTotal();

            //TryUpdateModel(order);
            order.OrderItems = orderList;
           
            db.Orders.Add(order);
            db.SaveChanges();

            shoppingCart.EmptyCart();
                
            return RedirectToAction("Complete", new { id = order.Id });
        }

        // GET: /Checkout/Complete
        public ActionResult Complete(int id)
        {
            var userId = User.Identity.GetUserId();
            // Validate customer owns this order
            bool isValid = db.Orders.Any(
                o => o.Id == id &&
                o.Customer.Id == userId);

            if (isValid)
            {
                return View(id);
            }
            else
            {
                return View("Error");
            }
        }

        private CreditCard MaskCard(CreditCard c)
        {

            var lastFour = c.CardNumber.Substring(c.CardNumber.Length - 4, 4);
            c.CardNumber = "************" + lastFour;

            return c;
        }
    }
}
﻿
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Medicus.Models;
using Medicus.ViewModels;
using System.Net;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;

namespace Medicus.Controllers
{
    public class AddressController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();
        
        // GET: Address
        public ActionResult Index()
        {
            ICollection<ShippingAddress> addresses = Helper.getCurrentUser().ShippingAddresses;
            return View(addresses);
        }

        // GET: Address/Create
        public ActionResult Create()
        {
            return View(new ShippingAddressViewModel());
        }

        // POST: Address/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Street,City,State,ZIP")] ShippingAddressViewModel addressModel)
        { 
            if (ModelState.IsValid)
            {
                ShippingAddress addr = new ShippingAddress();
                addr.City = addressModel.City;
                addr.Street = addressModel.Street;
                addr.State = addressModel.State;
                addr.ZIP = addressModel.ZIP;
                addr.CustomerId = Helper.getCurrentUser().Id;

                db.ShippingAddresses.Add(addr);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(addressModel);
        }

        // GET: Address/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // check address belongs to user
            ShippingAddress addr = Helper.getCurrentUser().ShippingAddresses.FirstOrDefault(x => x.Id == id);
            ShippingAddressViewModel addrModel = new ShippingAddressViewModel();

            if (addr == null)
            {
                return HttpNotFound();
            }

            addrModel.City = addr.City;
            addrModel.Street = addr.Street;
            addrModel.State = addr.State;
            addrModel.ZIP = addr.ZIP;
            addrModel.RowID = addr.RowID;

            return View(addrModel);
        }



        // POST: Address/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Street,City,State,ZIP,RowID")] ShippingAddressViewModel addressModel, int? id, byte[] RowID)
        {
            // check address belongs to user
            var currentUser = Helper.getCurrentUser();
            var addrToUpdate = db.ShippingAddresses.FirstOrDefault(x => x.Id == id && x.CustomerId == currentUser.Id);
            

            // if db entry deleted by anyone else
            if (addrToUpdate == null)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. The record was deleted by another user.");
                return View(addressModel);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    addrToUpdate.City = addressModel.City;
                    addrToUpdate.Street = addressModel.Street;
                    addrToUpdate.State = addressModel.State;
                    addrToUpdate.ZIP = addressModel.ZIP;
                    addrToUpdate.CustomerId = currentUser.Id;

                    db.Entry(addrToUpdate).OriginalValues["RowID"] = RowID;
                    db.Entry(addrToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    ModelState.AddModelError(string.Empty, "The record you attempted to edit "
                    + "was modified by another user after you got the original value. The "
                    + "edit operation was canceled and the current values in the database "
                    + "have been displayed. If you still want to edit this record, click "
                    + "the Save button again. Otherwise click the Back to List button.");

                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }

            // set view model's RowID to the just read from the db and send back to view 
            addressModel.RowID = addrToUpdate.RowID;
            return View(addressModel);
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            
            // check address belongs to user
            ShippingAddress addr = Helper.getCurrentUser().ShippingAddresses.FirstOrDefault(x => x.Id == id);

            if (addr == null)
            {
                return HttpNotFound();
            }
            return View(addr);
        }

        // POST: Address/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            // check address belongs to user
            var currentUser = Helper.getCurrentUser();
            var addr = db.ShippingAddresses.FirstOrDefault(x => x.Id == id && x.CustomerId == currentUser.Id);

            // check if entity not already removed
            if (addr != null)
            {
                    db.ShippingAddresses.Remove(addr);
                    db.SaveChanges();
            }

            return RedirectToAction("Index");
        }
    }
}
﻿
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Medicus.Models;
using Microsoft.AspNet.Identity;
using System.Data.Entity.Infrastructure;
using Medicus.ViewModels;
using System.Collections.ObjectModel;
using System;

namespace Medicus.Controllers
{
    public class CreditCardsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CreditCards
        public ActionResult Index()
        {
            ICollection<CreditCard> creditcards = Helper.getCurrentUser().CreditCards;
            ICollection<CreditCardShowViewModel> creditcardModels = new Collection<CreditCardShowViewModel>();
            foreach (var c in creditcards)
            {
                creditcardModels.Add(new CreditCardShowViewModel(c));
            }
            return View(creditcardModels);
        }

        // GET: CreditCards/Create
        public ActionResult Create()
        {
            CreditCardCreateViewModel creditcard = new CreditCardCreateViewModel();
            return View(creditcard);
        }

        // POST: CreditCards/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CardNumber,ExpireMonth,ExpireYear,FirstName,LastName")] CreditCardCreateViewModel creditcardModel)
        {
            if (ModelState.IsValid)
            {
                CreditCard creditcard = new CreditCard();
                creditcard.CardNumber = creditcardModel.CardNumber;
                creditcard.ExpireMonth = creditcardModel.ExpireMonth;
                creditcard.ExpireYear = creditcardModel.ExpireYear;
                creditcard.FirstName = creditcardModel.FirstName;
                creditcard.LastName = creditcardModel.LastName;
                creditcard.CustomerId = Helper.getCurrentUser().Id;

                db.CreditCards.Add(creditcard);
                db.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(creditcardModel);
        }

        // GET: CreditCards/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // check credit card belongs to user
            CreditCard creditcard = Helper.getCurrentUser().CreditCards.FirstOrDefault(x => x.Id == id);
            CreditCardEditViewModel creditcardModel = new CreditCardEditViewModel();

            if (creditcard == null)
            {
                return HttpNotFound();
            }

            mapToModel(creditcard, creditcardModel);

            return View(creditcardModel);
        }

        // POST: Address/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CardNumber,ExpireMonth,ExpireYear,FirstName,LastName,RowID")] CreditCardEditViewModel creditCardModel, int? id, byte[] RowID)
        {
            // check address belongs to user
            var currentUser = Helper.getCurrentUser();
            var ccToUpdate = db.CreditCards.FirstOrDefault(x => x.Id == id && x.CustomerId == currentUser.Id);


            // if db entry deleted by anyone else
            if (ccToUpdate == null)
            {
                ModelState.AddModelError(string.Empty, "Unable to save changes. The record was deleted by another user.");
                return View(creditCardModel);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    mapToEntity(creditCardModel, ccToUpdate);
                    ccToUpdate.CustomerId = currentUser.Id;

                    db.Entry(ccToUpdate).OriginalValues["RowID"] = RowID;
                    db.Entry(ccToUpdate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException)
                {
                    ModelState.AddModelError(string.Empty, "The record you attempted to edit "
                    + "was modified by another user after you got the original value. The "
                    + "edit operation was canceled and the current values in the database "
                    + "have been displayed. If you still want to edit this record, click "
                    + "the Save button again. Otherwise click the Back to List button.");

                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }
            }

            // set view model's RowID to the just read from the db and send back to view 
            creditCardModel.RowID = ccToUpdate.RowID;
            return View(creditCardModel);
        }

        // GET: Address/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // check address belongs to user
            ShippingAddress addr = Helper.getCurrentUser().ShippingAddresses.FirstOrDefault(x => x.Id == id);

            if (addr == null)
            {
                return HttpNotFound();
            }
            return View(addr);
        }

        // POST: Address/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            // check address belongs to user
            var currentUser = Helper.getCurrentUser();
            var addr = db.ShippingAddresses.FirstOrDefault(x => x.Id == id && x.CustomerId == currentUser.Id);

            // check if entity not already removed
            if (addr != null)
            {
                db.ShippingAddresses.Remove(addr);
                db.SaveChanges();
            }

            return RedirectToAction("Index");
        }


        private static void mapToModel(CreditCard creditcard, CreditCardEditViewModel creditcardModel)
        {
            creditcardModel.CardNumber = creditcard.CardNumber;
            creditcardModel.ExpireMonth = creditcard.ExpireMonth;
            creditcardModel.ExpireYear = creditcard.ExpireYear;
            creditcardModel.FirstName = creditcard.FirstName;
            creditcardModel.LastName = creditcard.LastName;
            creditcardModel.RowID = creditcard.RowID;
        }

        private static void mapToEntity(CreditCardEditViewModel creditcardModel, CreditCard creditcard)
        {
            creditcard.CardNumber = creditcardModel.CardNumber;
            creditcard.ExpireMonth = creditcardModel.ExpireMonth;
            creditcard.ExpireYear = creditcardModel.ExpireYear;
            creditcard.FirstName = creditcardModel.FirstName;
            creditcard.LastName = creditcardModel.LastName;
            creditcard.RowID = creditcardModel.RowID;
        }
    }
}

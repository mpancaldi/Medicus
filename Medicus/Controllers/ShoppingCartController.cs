﻿using System.Linq;
using System.Web.Mvc;
using Medicus.Models;
using Medicus.ViewModels;
using System.Diagnostics;

namespace Medicus.Controllers
{
    [AllowAnonymous]
    public class ShoppingCartController : Controller
    {
        private readonly ApplicationDbContext storeDB = new ApplicationDbContext();

        // GET: ShoppingCart/Index
        public ActionResult Index()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Set up our ViewModel
            var viewModel = new ShoppingCartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal(),
                CartItemCount = cart.GetCount()
            };

            return View(viewModel);
        }

        //GET: ShoppingCart/AddToCart/1234?qnt=1
        public ActionResult AddToCart(int id, int qnt)
        {
            if (qnt > 0 && qnt < 21)
            {
                for (int i = 0; i < qnt; i++)
                {
                    // Retrieve item from database
                    var addedItem = storeDB.Medicines.Single(medicine => medicine.Id == id);
                    // Add to shopping cart
                    var cart = ShoppingCart.GetCart(this.HttpContext);
                    cart.AddToCart(addedItem);
                }
            }
            return RedirectToAction("Index");
        }

        //AJAX
        [HttpPost]
        public ActionResult AddOneToCart(int id)
        {
            decimal medPrice = storeDB.Carts.Single(item => item.Id == id).Medicine.Price;
            
            // Add to shopping cart
            var cart = ShoppingCart.GetCart(this.HttpContext);
            int itemCount = cart.AddOneToCart(id);
            
            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                ItemSubTotal = itemCount * medPrice,
                DeleteId = id
            };

            return Json(results);
        }

        // AJAX
        // Decrease quantity one by one
        [HttpPost]
        public ActionResult RemoveOneFromCart(int id)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get medicine price to compute total price
            decimal medPrice = storeDB.Carts.Single(item => item.Id == id).Medicine.Price;

            int itemCount = cart.RemoveOneFromCart(id);

            // Display the confirmation message
            var results = new ShoppingCartRemoveViewModel
            {
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                ItemSubTotal = itemCount * medPrice,
                DeleteId = id
            };

            return Json(results);
        }


        // AJAX
        // Remove item from cart (set quantity to 0)
        [HttpPost]
        public ActionResult RemoveFromCart(int id)
        {
            // Remove the item from the cart
            var cart = ShoppingCart.GetCart(this.HttpContext);

            // Get the name of item to be removed
            string medName = storeDB.Carts.Single(item => item.Id == id).Medicine.Name;
            int itemCount = cart.RemoveFromCart(id);

            var results = new ShoppingCartRemoveViewModel
            {
                Message = Server.HtmlEncode(medName) +
                    " has been removed from your shopping cart.",
                CartTotal = cart.GetTotal(),
                CartCount = cart.GetCount(),
                ItemCount = itemCount,
                ItemSubTotal = 0,
                DeleteId = id
            };

            return Json(results);
        }


        // required for cart summary on the navbar
        [AjaxChildActionOnly]
        public ActionResult Summary()
        {
            var cart = ShoppingCart.GetCart(this.HttpContext);

            ViewData["CartCount"] = cart.GetCount();

            return PartialView("_SummaryPartial");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Medicus.Models;
using Medicus.ViewModels;
using Microsoft.AspNet.Identity;
using System.Diagnostics;

namespace Medicus.Controllers
{
    public class OrderController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Order
        public ActionResult Index(string searchString)
        {
            var userId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == userId);
            Order ord = new Order();
            ord.UpdateOrders();
            //Check if current user has completed orders
            if (db.Orders.Any(o => o.Customer.Id == userId)) 
            {
                //ICollection<Order> orders = currentUser.Orders;
                List<OrderViewModel> orders = new List<OrderViewModel>();
                var userOrders = currentUser.Orders;
                foreach (Order order in userOrders)
                {
                    orders.Add(new OrderViewModel(order));
                }

                if (!String.IsNullOrEmpty(searchString))
                {
                    int searchId;
                    if (int.TryParse(searchString, out searchId))
                    {
                        searchId = Int32.Parse(searchString);

                        if (db.Orders.Any(o => o.Id == searchId))
                        {
                            return RedirectToAction("Details", new { OrderId = searchId });
                        }
                    }
                    else
                    {
                        return View(orders.OrderByDescending(o => o.CreatedOn));
                    }
                }
                return View(orders.OrderByDescending(o => o.CreatedOn));
            }
            else
            {
                return RedirectToAction("NoOrders");
            }
            
        }

        public ActionResult Details(int OrderId)
        {
            var userId = User.Identity.GetUserId();
            ApplicationUser currentUser = db.Users.FirstOrDefault(user => user.Id == userId);
            if (db.Orders.Any(o => o.Customer.Id == userId))
            {
                ICollection<Order> orders = currentUser.Orders;
                if (db.Orders.Any(o => o.Id == OrderId))
                {
                    Order order = currentUser.Orders.Single(o => o.Id == OrderId);
                    OrderViewModel viewModel = new OrderViewModel(order);
                    ICollection<OrderItem> items = viewModel.OrderItems;
                    foreach (OrderItem item in items)
                    {
                        item.Medicine = db.Medicines.Single(m => m.Id == item.Medicine.Id);
                    }
                    return View(viewModel);
                }
                else
                {
                    return View("Index", orders.OrderByDescending(o => o.CreatedOn));
                }
            }
            else
            {
                return RedirectToAction("NoOrders");
            }
        }

        public ActionResult NoOrders()
        {
            return View();
        }
    }
}
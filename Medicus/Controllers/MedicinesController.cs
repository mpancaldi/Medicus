﻿using Medicus.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Medicus.Controllers
{
    [AllowAnonymous]
    public class MedicinesController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        // GET: Medicines
        public ActionResult Index(int? page, string searchString, int? filterCategory)
        {
            var medicines = from m in db.Medicines
                            select m;

            medicines = medicines.OrderBy(s => s.Name);

            int pageSize = 12;
            int pageNumber = (page ?? 1);

            if (!String.IsNullOrEmpty(searchString))
            {
                // search over all fields
                medicines = medicines.Where(s => s.Name.Contains(searchString) ||
                                                 s.Category.ToString() == searchString ||
                                                 s.Drug.Contains(searchString) ||
                                                 s.Indication.Contains(searchString) ||
                                                 s.ShortInfo.Contains(searchString) ||
                                                 s.Manufacturer.Contains(searchString) ||
                                                 s.Description.Contains(searchString)
                                                );
            }

            if (filterCategory.HasValue)
            {
                medicines = medicines.Where(x => (int) x.Category == filterCategory);
            }

            return View(medicines.ToPagedList(pageNumber, pageSize));

        }


        // GET: Medicines/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Medicine medicine = db.Medicines.Find(id);
            if (medicine == null)
            {
                return HttpNotFound();
            }
            return View(medicine);
        }
    }
}
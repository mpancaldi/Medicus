﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Medicus.Startup))]
namespace Medicus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
